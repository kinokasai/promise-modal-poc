[@bs.scope "document"] [@bs.val] external body: Dom.element = "body";

[@react.component]
let make = (~children, ~onModalClose) => {
  let modalRef = React.useRef(Js.Nullable.null);

  ReactDOMRe.createPortal(
    <div className="modal-container" role="dialog" ariaModal=true>
      <div className="modal-content" ref={ReactDOMRe.Ref.domRef(modalRef)}>
        children
      </div>
      <button onClick={_ => onModalClose()}>
        "Close"->ReasonReact.string
      </button>
    </div>,
    body,
  );
};
