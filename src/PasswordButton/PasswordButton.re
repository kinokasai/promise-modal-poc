type action =
  | OpenModal(Js.Fn.arity1(string => unit))
  | CloseModal
  | SetPassword(string)
  | SendPassword;

type state = {
  password: string,
  visibleModal: bool,
  resolve: option(Js.Fn.arity1(string => unit)),
};

[@react.component]
let make = () => {
  let (state, dispatch) =
    React.useReducer(
      (state, action) =>
        switch (action) {
        | OpenModal(resolve) => {
            ...state,
            visibleModal: true,
            resolve: Some(resolve),
          }
        | CloseModal => {...state, visibleModal: false}
        | SetPassword(password) => {...state, password}
        | SendPassword =>
          switch (state.resolve) {
          | None => {...state, visibleModal: false}
          | Some(resolve) =>
            resolve(. state.password);
            {...state, visibleModal: false};
          }
        },
      {password: "", visibleModal: false, resolve: None},
    );

  let prompt_password = () => {
    let promise =
      Js.Promise.make((~resolve, ~reject) => dispatch(OpenModal(resolve)));
    promise;
  };
  <div>
    <button
      onClick={_ => SdkMockup.return_password(~prompt_password) |> ignore}>
      "Transfer"->ReasonReact.string
    </button>
    {!state.visibleModal
       ? ReasonReact.null
       : <Modal onModalClose={_ => dispatch(CloseModal)}>
           <p> "Password"->ReasonReact.string </p>
           <input
             type_="text"
             onChange={event => {
               let value = ReactEvent.Form.target(event)##value;
               dispatch(SetPassword(value));
             }}
           />
           <button onClick={_ => {dispatch(SendPassword)}}>
             "Send"->ReasonReact.string
           </button>
         </Modal>}
  </div>;
};
