let return_password = (~prompt_password) => {
  let promise = prompt_password();
  Js.Promise.(then_(pwd => Js.log(pwd) |> resolve, promise));
};
